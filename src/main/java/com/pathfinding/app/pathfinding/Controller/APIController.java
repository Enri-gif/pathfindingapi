package com.pathfinding.app.pathfinding.Controller;

import java.util.ArrayList;

import com.pathfinding.app.pathfinding.Dij;
import com.pathfinding.app.pathfinding.Matrix;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class APIController {

    @GetMapping(value = "/")
    public ArrayList<Integer> GetDefault(){
        Dij dij = new Dij();
        Matrix matrix = new Matrix(64);

        dij.CalcDist(matrix.arr, 2, 54);
        return dij.path;
    }

    @GetMapping(value = "path")
    public ArrayList<Integer> GetPath(int start, int stop){
        Dij dij = new Dij();
        Matrix matrix = new Matrix(64);

        dij.CalcDist(matrix.arr, start, stop);
        return dij.path;
    }
}
