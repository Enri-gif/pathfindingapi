package com.pathfinding.app.pathfinding;

public class Matrix {
    public int arr[][];
    public int size;

    // Initialize the matrix
    public Matrix(int size)
    {
        int i,j;
        this.size = size;
        arr = new int[size][size];
        for(i = 0; i < size; i++)
        {
            for(j = 0; j < size; j++)
            {
                if (i < size)
                    if (i == j-1 || i == j+1 || i == j-Math.sqrt(size) || i == j+Math.sqrt(size))
                        if (j%Math.sqrt(size) == 0 && i == j-1 || i%Math.sqrt(size) == 0 && j == i-1)
                            arr[i][j] = 0;//i1 j9
                        else
                            arr[i][j] = 1;
                    else
                        arr[i][j] = 0;
                else
                    arr[i][j] = 0;

            }
        }
    }

    // Add edges
    public void addEdge(int i, int j)
    {
        arr[i][j] = 1;
        arr[j][i] = 1;
    }

    // Remove edges
    public void removeEdge(int i, int j)
    {
        arr[i][j] = 0;
        arr[j][i] = 0;
    }

    // Print the matrix
    /*
    public String toString()
    {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < size; i++)
        {
            s.append(i + ": ");
            for (int j : arr[i])
            {
                s.append((j ? 1 : 0) + " ");
            }
            s.append("\n");
        }
        return s.toString();
    }*/
}
